module gitlab.com/rolandknight/docsy

go 1.16

require (
	github.com/FortAwesome/Font-Awesome v4.7.0+incompatible // indirect
	github.com/twbs/bootstrap v4.6.0+incompatible // indirect
)
